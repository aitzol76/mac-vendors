**Manuf** is a file which contains a vendors and manufacturers list ordered by **MAC** address or **OUI**

This file has been obtained from the mirror of the **Wireshark Foundation** in [Github](https://github.com/wireshark/wireshark), and is updated weekly from its location.

The original project can be found at [code.wireshark.org](https://code.wireshark.org/)

**Wireshark** is a registered trademark of the **Wireshark Foundation**.

**Wireshark** is freely available as open source, and is released under the GNU General Public License version 2. 

Other interesting sites for manufacturers search by **MAC** or **OUI**:

* [hwaddress.com](https://hwaddress.com/)

* [macvendors.co](https://macvendors.co)
